//Calculations
    
function Addition() {

	 var x= +$("#num1").val();
	 var y= +$("#num2").val();		 
	 my = $("#resultAdd");
	 my.val(x+y);
	

}
function Subtract() {

	 var x= +$("#num1s").val();
	 var y= +$("#num2s").val();		 
	 my = $("#resultSub");
	 my.val(x-y);
	 
}

function Multiplication() {

	 var x= +$("#num1m").val();
	 var y= +$("#num2m").val();		 
	 my = $("#resultMul");
	 my.val(x*y);
}

function Division() {

	 var x= +$("#num1d").val();
	 var y= +$("#num2d").val();		 
	 my = $("#resultDiv");
	 my.val(x/y);
}
jQuery('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});


//Pronouncing the main menu contents
    function Add(){
          var msg = new SpeechSynthesisUtterance("Addition");
    window.speechSynthesis.speak(msg);
          }
          
          function Sub(){
          var msg1 = new SpeechSynthesisUtterance("Subtraction");
    window.speechSynthesis.speak(msg1);
          }
          
          function Mul(){
          var msg2 = new SpeechSynthesisUtterance("Multiplication");
    window.speechSynthesis.speak(msg2);
          }
          
          function Div(){
          var msg3 = new SpeechSynthesisUtterance("Division");
    window.speechSynthesis.speak(msg3);
          }
          

//Pronouncing the result of each calculation         
          function AddResult(){
              var x = document.getElementById("num1").value;
              var y = document.getElementById("num2").value;
              var a = parseInt(x);
              var b = parseInt(y);
              var result = a+b;
    var msg4 = new SpeechSynthesisUtterance(a+"Plus"+b+"Is Equal to"+result);
    window.speechSynthesis.speak(msg4);
          }
          
          function SubResult(){
              var x = document.getElementById("num1s").value;
              var y = document.getElementById("num2s").value;
              var a = parseInt(x);
              var b = parseInt(y);
              var result = a-b;
    var msg5 = new SpeechSynthesisUtterance(a+"Minus"+b+"Is Equal to"+result);
    window.speechSynthesis.speak(msg5);
          }
          
          function MulResult(){
              var x = document.getElementById("num1m").value;
              var y = document.getElementById("num2m").value;
              var a = parseInt(x);
              var b = parseInt(y);
              var result = a*b;
    var msg6 = new SpeechSynthesisUtterance(a+"Multiply by"+b+"Is Equal                                             to"+result);
    window.speechSynthesis.speak(msg6);
          }
          
          function DivResult(){
              var x = document.getElementById("num1d").value;
              var y = document.getElementById("num2d").value;
              var a = parseInt(x);
              var b = parseInt(y);
              var result = a/b;
    var msg7 = new SpeechSynthesisUtterance(a+"Divided by"+b+"Is Equal                                               to"+result);
    window.speechSynthesis.speak(msg7);
          }

